//This file was generated from (Academic) UPPAAL 4.0.13 (rev. 4577), September 2010

/*

*/
//NO_QUERY

/*
Deadlock Freeness
*/
A[] not deadlock 

/*

*/
A[] client.req_sent imply delta_post==0

/*

*/
A[] client.req_sent imply delta_post<=timeout

/*

*/
//NO_QUERY

/*

*/
A[] server.get_event imply delta_get==0 \


/*

*/
A[] server.get_on imply delta_get<=time_on\


/*

*/
A[] server.get_off imply (delta_get>=time_on and delta_get<=max_delta_get)\


/*

*/
//NO_QUERY

/*

*/
A[] client.req_off imply (delta_post>=timeout and delta_post<=max_delta_post)

/*

*/
//NO_QUERY

/*

*/
//NO_QUERY

/*

*/
A[] glue.trans_succ imply delta_post>=serve_time

/*

*/
A[] glue.trans_fail_1 imply delta_post<=serve_time

/*

*/
//NO_QUERY

/*

*/
A[] glue.trans_succ imply (client.req_on and delta_post<=timeout and (server.res_event_1 or server.res_event_2) and delta_get<=time_on+serve_time)

/*

*/
//NO_QUERY

/*

*/
A[] glue.trans_fail_1 imply (client.req_on and delta_post==timeout and ((server.get_off and delta_get-time_on>=timeout) or (server.get_on and delta_get==0))) 

/*

*/
A[] glue.trans_fail_1 imply (server.get_off or server.get_on)

/*

*/
A[] glue.trans_fail_1 imply (not server.proc_req)

/*

*/
//NO_QUERY

/*

*/
A[] glue.trans_fail_2 imply (client.req_on and delta_post==timeout and server.proc_req and delta_get<=serve_time)

/*

*/
//NO_QUERY

/*

*/
E<> glue.trans_fail_2

/*

*/
E<> glue.trans_fail_1

/*

*/
E<> glue.trans_succ
