//This file was generated from (Academic) UPPAAL 4.0.13 (rev. 4577), September 2010

/*
Deadlock Freeness
*/
A[] not deadlock

/*

*/
A[] glue.glue_init + glue.glue_get + glue.glue_get_post + glue.glue_post <= 1

/*

*/
//NO_QUERY

/*

*/
//NO_QUERY

/*
reachability
*/
E<> poster.post_on

/*

*/
E<> poster.post_end

/*
reachability
*/
E<> getter.get_on

/*

*/
E<> getter.get_end

/*

*/
E<> glue.glue_get

/*

*/
E<> glue.glue_get_post

/*

*/
E<> glue.glue_post

/*

*/
E<> (poster.post_on and getter.get_on) imply (glue.glue_get_post and glue.trans_succ)

/*

*/
//NO_QUERY

/*

*/
(poster.post_on and C_lease < lease) --> (glue.glue_post or glue.glue_get_post)

/*

*/
(getter.get_on and C_timeout < timeout) --> (glue.glue_get or glue.glue_get_post)

/*

*/
(poster.post_on and getter.get_on)  and ((C_timeout<timeout and C_lease == 0)  or (C_lease<lease and C_timeout == 0)) --> (glue.trans_succ and is_trans_succ == true)

/*

*/
glue.glue_get_post --> glue.trans_succ

/*

*/
A[] (a and b) imply not poster.post_end

/*

*/
A[] (d and e) imply not getter.get_end

/*

*/
A[] a imply not poster.post_end

/*

*/
A[] (a and b and d and e) imply (C_timeout < timeout and C_lease < lease)

/*

*/
A[] (is_trans_succ == true) imply (C_timeout < timeout or C_lease < lease)

/*

*/
E<> poster.post_succ

/*

*/
A[] a imply not poster.post_end

/*

*/
(poster.post_on and getter.get_on) --> poster.post_succ

/*

*/
//NO_QUERY

/*

*/
//NO_QUERY

/*

*/
FINAL PROPERTIES

/*

*/
----------------

/*

*/
A[] poster.post_event imply C_post==0

/*

*/
A[] poster.post_on imply C_post<=lease

/*

*/
A[] poster.post_off imply (C_post>=lease and C_post<=post_interval)

/*

*/
E<> poster.post_end_event and C_post<lease

/*

*/
//NO_QUERY

/*

*/
A[] getter.get_event imply C_get==0 

/*

*/
A[] getter.get_on imply C_get<=timeout

/*

*/
A[] getter.get_off imply (C_get>=timeout and C_get<=get_interval)

/*

*/
A<> getter.get_end_event imply C_get==timeout

/*

*/
//NO_QUERY

/*

*/
A[] glue.trans_succ imply (poster.post_on and getter.get_on and (C_post==0 or C_get==0))

/*

*/
A[] glue.trans_fail imply (poster.post_on and C_post==lease and getter.get_off and C_get-timeout>=lease)

/*

*/
A[] getter.no_trans imply (getter.get_on and C_get==timeout and poster.post_off and C_post-lease>=timeout)

/*

*/
//NO_QUERY

/*

*/
TESTS

/*

*/
-----

/*

*/
A[] not getter.get_on imply (C_get>=timeout and C_get<=get_interval)

/*

*/
A[] glue.trans_succ imply ((poster.post_on and getter.get_event) or (getter.get_on and poster.post_event))

/*

*/
A[] glue.trans_fail imply (C_post==lease and not getter.get_on and C_get-timeout>=lease)

/*

*/
E<> getter.no_trans

/*

*/
E<> glue.trans_succ

/*

*/
E<> glue.trans_fail
